import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    compras: 0,
    poblacion: 0,
    area: 0,
    paises: [],
    likes: 0,
    paisesAgregados: [],
    paisesConLike: []
  },
  getters: {
    promedio(state) {
      if (state.compras === 0) {
        return 0
      }
      return parseInt(state.poblacion / state.compras)
    },
    ultimosPaisesAgregados(state) {
      return state.paisesAgregados.slice(Math.max(state.paisesAgregados.length - 5, 0))
    }
  },
  mutations: {
    nuevaCompra(state, precio) {
      state.compras = state.compras + 1
      state.poblacion = state.poblacion + precio
    },
    agregarArea(state, area) {
      state.area = state.area + area
    },
    getPaises(state, paises) {
      state.paises = paises
    },
    setLikes(state, like) {
      if (like) state.likes++
      else state.likes--
    },
    agregarPais(state, pais) {
      state.paisesAgregados.push({ pais })
    },
    reset(state) {
      state.compras = 0
      state.poblacion = 0
      state.area = 0
      state.paises = []
      state.likes = 0
      state.paisesAgregados = []
    },
    agregarLike(state, index){
      state.paisesConLike.push(state.paises[index])
    },
  },
  actions: {
    async getAllCountries(context) {
      return await axios.get('https://restcountries.eu/rest/v2/regionalbloc/usan').then(
        response => {
          let paises = response.data
          context.commit('getPaises', paises)
        })
    }
  }
})